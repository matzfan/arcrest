## master (unreleased)

## [1.1.0] - 2024-06-29

### Bug fixes

* Curb gem now a runtime dependency

## [1.0.0] - 2024-06-28

### Bug fixes

* Curb gem now a development dependency
