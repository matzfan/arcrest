# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
require 'arcrest'

PROXIED_ENDPOINT = 'https://gojdippmaps.azurewebsites.net/proxy.ashx?https://maps.gov.je/arcgis/rest/services'
REFERER = 'https://www.gov.je/'
