# frozen_string_literal: true

endpoint = 'https://sampleserver6.arcgisonline.com/ArcGIS/rest/services/' # v11.1
feature_layer_url = "#{endpoint}/Military/MapServer/2"
proxied_url = "#{PROXIED_ENDPOINT}/Historic_Buildings/Historic_Buildings/FeatureServer/0"

describe ArcREST::Layer do
  let(:feature_layer) { described_class.new(feature_layer_url) }
  let(:features) { feature_layer.features(resultRecordCount: 2) } # limit 2
  let(:proxied_feature_layer) { described_class.new(proxied_url, headers: { referer: REFERER }) }

  describe '#new(url)' do
    it 'can be instantiated with a url string' do
      expect(proxied_feature_layer.class).to eq described_class
    end
  end

  context 'with meta-methods' do
    describe '#id' do
      it 'returns the layer id' do
        expect(feature_layer.id).to eq 2
      end
    end

    describe '#name' do
      it 'returns the layer name' do
        expect(feature_layer.name).to eq 'Units'
      end
    end

    describe '#type' do
      it 'returns the layer type' do
        expect(feature_layer.type).to eq 'Feature Layer'
      end
    end

    describe '#drawing_info' do
      it 'returns a Hash including a "renderer" key' do
        expect(feature_layer.drawing_info.keys).to include 'renderer'
      end
    end

    describe '#fields' do
      it 'returns a list of field hash data' do
        expect(feature_layer.fields.all? { |f| f.instance_of? Hash }).to be true
      end
    end

    describe '#max_record_count' do
      it 'returns the limit on number of features returned from a query' do
        expect(feature_layer.max_record_count).to eq 1000
      end
    end
  end

  context 'with a Feature Layer' do
    describe '#object_ids' do
      it 'returns a list of object Ids' do
        expect(feature_layer.object_ids.all? { |i| i.is_a? Integer }).to be true
      end
    end

    describe '#count' do
      it 'returns the (integer) number of features' do
        expect(feature_layer.count.is_a?(Integer)).to be true
      end
    end

    describe '#query(options)' do
      it 'raises InvalidOption error with an invalid option key' do
        expect { feature_layer.query(invalidOption: true) }.to raise_error ArcREST::InvalidOption
      end

      it 'raises BadQuery with an invalid where: String' do
        expect { feature_layer.query(where: 'invalid SQL') }.to raise_error ArcREST::BadQuery
      end

      it 'returns a list of hashes, whose keys include fields & features' do
        keys = %w[fields features]
        opts = { where: '1=1', resultRecordCount: 1 }
        keys.all? { |k| expect(feature_layer.query(opts).keys).to include k }
      end
    end

    describe '#features(options)' do
      it 'returns a list of hashes, whose keys are: "geometry", "attributes"' do
        features.map(&:keys).each do |keys|
          expect(keys.sort).to eq %w[attributes geometry]
        end
      end
    end
  end
end
