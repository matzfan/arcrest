# frozen_string_literal: true

require 'net/http'
require 'open-uri'
require 'json'

require_relative 'curlable'

module ArcREST
  # superclass
  class Server
    include Curlable # for #curl_get

    ENDPOINT_REGEX = %r{/arcgis/rest/services/?}i
    BAD_ENDPOINT = 'Invalid ArcGIS endpoint'

    attr_reader :url, :json, :version

    def initialize(url, headers = {})
      @url = url
      @headers = headers
      validate_endpoint_url
      @server_url = server_url
      @json = json_
      @version = version_
    end

    protected

    def server_url
      @url[-1] == '/' ? @url[0..-2] : @url
    end

    def json_
      parse_json
    end

    def version_
      parse_json['currentVersion'] # subclasses use server uri
    end

    def parse_json(url = @server_url, options = {})
      JSON.parse get(url, options)
    end

    def validate_endpoint_url
      raise ArgumentError, BAD_ENDPOINT unless @url =~ ENDPOINT_REGEX
    end

    def add_json_param_to(hash)
      { f: 'json' }.merge(hash)
    end

    def get(url, options = {})
      curl_get(query_string(url, options), @headers)
    end

    def query_string(url, options)
      "#{url}?#{URI.encode_www_form(add_json_param_to(options))}"
    end
  end
end
