# frozen_string_literal: true

require 'arc_rest/attributable' # module
require 'arc_rest/version'
require 'arc_rest/server'
require 'arc_rest/catalog'
require 'arc_rest/service'
require 'arc_rest/layer'

# namespace
module ArcREST; end
